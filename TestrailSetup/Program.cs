﻿using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Firefox;
using System;
using System.Configuration;

namespace QaLeadTestrailSetup
{
    class Program
    {

        static void Main(string[] args)
        {
            String username = ConfigurationManager.AppSettings["username"];
            String password = ConfigurationManager.AppSettings["password"];
            String milestoneName = ConfigurationManager.AppSettings["milestoneName"];
            String dueDate = ConfigurationManager.AppSettings["dueDate"];

            IWebDriver driver = new FirefoxDriver(AppDomain.CurrentDomain.BaseDirectory);
            driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(10);
            driver.Navigate().GoToUrl("https://nmbrs.testrail.com");
            driver.Manage().Window.Maximize();
            driver.FindElement(By.XPath("//*[@id='name']")).SendKeys(username);
            driver.FindElement(By.XPath("//*[@id='password']")).SendKeys(password);
            driver.FindElement(By.XPath("//*[@id='content']/form/div[4]/button")).Click();

            //QA update
            System.Threading.Thread.Sleep(2000);
            driver.FindElement(By.XPath("//*[@id='project-5']/div[2]/div[1]/a")).Click();

            //Milestone
            driver.FindElement(By.XPath("//*[@id='header']/ul/li[4]/a")).Click();
            driver.FindElement(By.XPath("//*[@id='sidebar']/div[2]/a")).Click();
            driver.FindElement(By.XPath("//*[@id='name']")).SendKeys(milestoneName);
            driver.FindElement(By.XPath("//*[@id='due_on']")).SendKeys(dueDate);
            driver.FindElement(By.XPath("//*[@id='form']/div[10]/button")).Click();

            //Test Run - Normal
            driver.FindElement(By.XPath("//*[@id='header']/ul/li[5]/a")).Click();
            driver.FindElement(By.XPath("//*[@id='sidebar']/div/a[1]/span")).Click();
            driver.FindElement(By.XPath("//*[@id='name']")).Clear();
            driver.FindElement(By.XPath("//*[@id='name']")).SendKeys(milestoneName);
            driver.FindElement(By.XPath("//*[@id='milestone_id']")).SendKeys(milestoneName);
            driver.FindElement(By.XPath("//*[@id='includeSpecific']")).Click();
            driver.FindElement(By.XPath("//*[@id='includeSpecificInfo']/a")).Click();
            driver.FindElement(By.XPath("//*[@id='selectCasesTreeAll']")).Click();
            driver.FindElement(By.XPath("//*[@id='selectCasesNode-24912']/input")).Click();
            driver.FindElement(By.XPath("//*[@id='selectCasesNode-16571']/input")).Click();
            System.Threading.Thread.Sleep(3000);
            driver.FindElement(By.XPath("//*[@id='selectCasesSubmit']")).Click();
            System.Threading.Thread.Sleep(1000);
            driver.FindElement(By.XPath("//*[@id='form']/div[9]/button")).Click();

            //Test Run - After Update
            driver.FindElement(By.XPath("//*[@id='header']/ul/li[5]/a")).Click();
            driver.FindElement(By.XPath("//*[@id='sidebar']/div/a[1]/span")).Click();
            driver.FindElement(By.XPath("//*[@id='name']")).Clear();
            driver.FindElement(By.XPath("//*[@id='name']")).SendKeys(milestoneName + " after update");
            driver.FindElement(By.XPath("//*[@id='milestone_id']")).SendKeys(milestoneName);
            driver.FindElement(By.XPath("//*[@id='includeSpecific']")).Click();
            driver.FindElement(By.XPath("//*[@id='includeSpecificInfo']/a")).Click();
            driver.FindElement(By.XPath("//*[@id='selectCasesTreeNone']")).Click();
            driver.FindElement(By.XPath("//*[@id='selectCasesNode-16571']/input")).Click();
            System.Threading.Thread.Sleep(3000);
            driver.FindElement(By.XPath("//*[@id='selectCasesSubmit']")).Click();
            System.Threading.Thread.Sleep(1000);
            driver.FindElement(By.XPath("//*[@id='form']/div[9]/button")).Click();

            //Update Process
            System.Threading.Thread.Sleep(1000);
            driver.FindElement(By.XPath("//*[@id='return']/a")).Click();
            driver.FindElement(By.XPath("//*[@id='project-6']/div[2]/div[1]/a")).Click();

            //Milestone
            driver.FindElement(By.XPath("//*[@id='header']/ul/li[4]/a")).Click();
            driver.FindElement(By.XPath("//*[@id='sidebar']/div[2]/a")).Click();
            driver.FindElement(By.XPath("//*[@id='name']")).SendKeys(milestoneName);
            driver.FindElement(By.XPath("//*[@id='due_on']")).SendKeys(dueDate);
            driver.FindElement(By.XPath("//*[@id='form']/div[10]/button")).Click();

            //Test Run - Normal
            driver.FindElement(By.XPath("//*[@id='header']/ul/li[5]/a")).Click();
            driver.FindElement(By.XPath("//*[@id='sidebar']/div/a[1]/span")).Click();
            driver.FindElement(By.XPath("//*[@id='name']")).Clear();
            driver.FindElement(By.XPath("//*[@id='name']")).SendKeys(milestoneName);
            driver.FindElement(By.XPath("//*[@id='milestone_id']")).SendKeys(milestoneName);
            driver.FindElement(By.XPath("//*[@id='includeSpecific']")).Click();
            driver.FindElement(By.XPath("//*[@id='includeSpecificInfo']/a")).Click();
            driver.FindElement(By.XPath("//*[@id='selectCasesTreeAll']")).Click();
            driver.FindElement(By.XPath("//*[@id='selectCasesNode-27198']/input")).Click();
            System.Threading.Thread.Sleep(3000);
            driver.FindElement(By.XPath("//*[@id='selectCasesSubmit']")).Click();
            System.Threading.Thread.Sleep(1000);
            driver.FindElement(By.XPath("//*[@id='form']/div[9]/button")).Click();

            driver.Quit();
        }
    }
}
